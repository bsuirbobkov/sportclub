﻿using System;
using System.Windows.Forms;
using SportClub.App.Forms;
using SportClub.Configuration;

namespace SportClub.App
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            ConfigurationManager.BuildConfiguration();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
