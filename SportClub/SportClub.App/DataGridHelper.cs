﻿using System.Windows.Forms;
using SportClub.Logic.Services;

namespace SportClub.App
{
    public static class DataGridHelper
    {
        public static void Set(DataGridView dataGrid, BindingSource bindingSource)
        {
            dataGrid.DataSource = bindingSource;
            dataGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGrid.AutoGenerateColumns = true;
            dataGrid.AllowUserToAddRows = false;
            dataGrid.RowHeadersVisible = false;
            dataGrid.AllowUserToResizeRows = false;
            dataGrid.AllowUserToResizeColumns = false;
            dataGrid.MultiSelect = false;
            dataGrid.ReadOnly = true;
        }

        public static void HideColumns(DataGridView dataGrid, params string[] columns)
        {
            foreach (var column in columns)
            {
                if (dataGrid.Columns[column] != null)
                {
                    dataGrid.Columns[column].Visible = false;
                }
            }
        }

        public static TItem Get<TItem>(DataGridView dataGrid)
        {
            var item = (TItem)dataGrid.SelectedRows[0].DataBoundItem;
            return item;
        }
    }
}