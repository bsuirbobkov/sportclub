﻿using System;
using SportClub.App.Forms;

namespace SportClub.App
{
    public partial class MainForm : System.Windows.Forms.Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void FormTest_Load(object sender, EventArgs e)
        {
        }

        private void manage_athletes_Click(object sender, EventArgs e)
        {
            var form = new AthleteForm();
            form.ShowDialog();
        }

        private void manage_coaches_Click(object sender, EventArgs e)
        {
            var form = new CoachForm();
            form.ShowDialog();
        }

        private void manage_competitions_Click(object sender, EventArgs e)
        {
            var form = new CompetitionForm();
            form.ShowDialog();
        }

        private void manage_traumas_Click(object sender, EventArgs e)
        {
            var form = new TraumaForm();
            form.ShowDialog();
        }
    }
}
