﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SportClub.Logic.Entities;
using SportClub.Logic.Models;
using SportClub.Logic.Services;

namespace SportClub.App.Forms
{
    public partial class CompetitionEditForm : Form
    {
        private readonly CompetitionService competitionService = new CompetitionService();
        private readonly GenderService genderService = new GenderService();
        private readonly SkillService skillService = new SkillService();
        private readonly AthleteService athleteService = new AthleteService();

        private readonly bool editMode;
        private readonly CompetitionModel model;
        private readonly FilterModel filter;

        public CompetitionEditForm(CompetitionModel model, FilterModel filter)
        {
            editMode = model != null;
            this.model = model;
            this.filter = filter;
            InitializeComponent();
        }

        private void CompetitionEditForm_Load(object sender, EventArgs e)
        {
            SetModel();
        }

        private void SetModel()
        {
            var genders = genderService.GetAll();
            var skills = skillService.GetAll();
            var athletes = athleteService.GetForCompetition(model?.Id ?? 0, filter);

            name_textbox.Text = model?.Name;
            date_datepicker.Value = model?.Date ?? DateTime.Now;
            minAge_textbox.Text = model?.MinAge.ToString();
            maxAge_textbox.Text = model?.MaxAge.ToString();

            FormHelper.SetComboBox(
                minSkill_combobox,
                skills,
                model?.MinSkillId,
                nameof(SkillEntity.Id),
                nameof(SkillEntity.Name)
            );

            FormHelper.SetComboBox(
                maxSkill_combobox,
                skills,
                model?.MaxSkillId,
                nameof(SkillEntity.Id),
                nameof(SkillEntity.Name)
            );

            FormHelper.SetComboBox(
                gender_combobox,
                genders,
                model?.GenderId,
                nameof(GenderEntity.Id),
                nameof(GenderEntity.Name)
            );

            FormHelper.SetCheckList(checkedListBox, athletes);
        }

        private void create_button_Click(object sender, System.EventArgs e)
        {
            if (!IsValid())
            {
                MessageBox.Show("Введите значение");
                return;
            }
            if (editMode)
            {
                competitionService.Update(GetModel(), GetCheckedAthletes());
            }
            else
            {
                competitionService.Create(GetModel(), GetCheckedAthletes());
            }
            Close();
        }

        private CompetitionModel GetModel()
        {
            var gender = (GenderEntity)gender_combobox.SelectedItem;
            var minSkill = (SkillEntity)minSkill_combobox.SelectedItem;
            var maxSkill = (SkillEntity)maxSkill_combobox.SelectedItem;

            return new CompetitionModel
            {
                Id = model?.Id ?? 0,
                Name = name_textbox.Text,
                Date = date_datepicker.Value,
                MinSkillId = minSkill.Id,
                MaxSkillId = maxSkill.Id,
                GenderId = gender.Id,
                MinAge = int.Parse(minAge_textbox.Text),
                MaxAge = int.Parse(maxAge_textbox.Text)
            };
        }

        private IReadOnlyCollection<CheckItemModel> GetCheckedAthletes()
        {
            var result = new List<CheckItemModel>();
            foreach (var item in checkedListBox.CheckedItems)
            {
                result.Add((CheckItemModel)item);
            }
            return result;
        }

        private bool IsValid()
        {
            if (name_textbox.Text.Length == 0)
            {
                return false;
            }
            if (minAge_textbox.Text.Length == 0)
            {
                return false;
            }
            if (maxAge_textbox.Text.Length == 0)
            {
                return false;
            }
            if (minSkill_combobox.SelectedItem == null)
            {
                return false;
            }
            if (maxSkill_combobox.SelectedItem == null)
            {
                return false;
            }
            if (gender_combobox.SelectedItem == null)
            {
                return false;
            }
            return true;
        }

        private void minAge_textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            FormHelper.NumberOnly(sender, e);
        }

        private void maxAge_textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            FormHelper.NumberOnly(sender, e);
        }
    }
}
