﻿using System.Windows.Forms;
using SportClub.Logic.Models;
using SportClub.Logic.Services;

namespace SportClub.App.Forms
{
    public partial class AthleteForm : Form
    {
        private readonly AthleteService athleteService = new AthleteService();

        public AthleteForm()
        {
            InitializeComponent();
        }

        private void AthleteForm_Load(object sender, System.EventArgs e)
        {
            ReloadContent();
        }

        private async void ReloadContent()
        {
            var dataSource = await DataSourceHelper.Create(() => athleteService.GetAll());
            DataGridHelper.Set(dataGridView, dataSource);
            DataGridHelper.HideColumns(dataGridView,
                nameof(AthleteModel.Id),
                nameof(AthleteModel.GenderId),
                nameof(AthleteModel.CoachId),
                nameof(AthleteModel.SkillId)
            );
        }

        private void create_button_Click(object sender, System.EventArgs e)
        {
            var form = new AthleteEditForm(null);
            form.ShowDialog();
            ReloadContent();
        }

        private void edit_button_Click(object sender, System.EventArgs e)
        {
            if (dataGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите запись");
                return;
            }
            var model = DataGridHelper.Get<AthleteModel>(dataGridView);
            var editForm = new AthleteEditForm(model);
            editForm.ShowDialog();
            ReloadContent();
        }

        private void delete_button_Click(object sender, System.EventArgs e)
        {
            if (dataGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите запись");
                return;
            }
            var model = DataGridHelper.Get<AthleteModel>(dataGridView);
            athleteService.Delete(model.Id);
            ReloadContent();
        }
    }
}
