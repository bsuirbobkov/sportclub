﻿namespace SportClub.App.Forms
{
    partial class CompetitionEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.name_textbox = new System.Windows.Forms.TextBox();
            this.date_datepicker = new System.Windows.Forms.DateTimePicker();
            this.minSkill_combobox = new System.Windows.Forms.ComboBox();
            this.maxSkill_combobox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.minAge_textbox = new System.Windows.Forms.TextBox();
            this.maxAge_textbox = new System.Windows.Forms.TextBox();
            this.gender_combobox = new System.Windows.Forms.ComboBox();
            this.checkedListBox = new System.Windows.Forms.CheckedListBox();
            this.create_button = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.name_textbox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.date_datepicker, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.minSkill_combobox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.maxSkill_combobox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.minAge_textbox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.maxAge_textbox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.gender_combobox, 1, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(842, 240);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Название";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(244, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Дата проведения";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(244, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Минимальный уровень мастерства";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(244, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Максимальный уровень мастерства";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(244, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Минимальный возраст";
            // 
            // name_textbox
            // 
            this.name_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.name_textbox.Location = new System.Drawing.Point(253, 6);
            this.name_textbox.Name = "name_textbox";
            this.name_textbox.Size = new System.Drawing.Size(586, 22);
            this.name_textbox.TabIndex = 5;
            // 
            // date_datepicker
            // 
            this.date_datepicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.date_datepicker.Location = new System.Drawing.Point(253, 40);
            this.date_datepicker.Name = "date_datepicker";
            this.date_datepicker.Size = new System.Drawing.Size(586, 22);
            this.date_datepicker.TabIndex = 6;
            // 
            // minSkill_combobox
            // 
            this.minSkill_combobox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.minSkill_combobox.FormattingEnabled = true;
            this.minSkill_combobox.Location = new System.Drawing.Point(253, 72);
            this.minSkill_combobox.Name = "minSkill_combobox";
            this.minSkill_combobox.Size = new System.Drawing.Size(586, 24);
            this.minSkill_combobox.TabIndex = 7;
            // 
            // maxSkill_combobox
            // 
            this.maxSkill_combobox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.maxSkill_combobox.FormattingEnabled = true;
            this.maxSkill_combobox.Location = new System.Drawing.Point(253, 106);
            this.maxSkill_combobox.Name = "maxSkill_combobox";
            this.maxSkill_combobox.Size = new System.Drawing.Size(586, 24);
            this.maxSkill_combobox.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(244, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Максимальный возраст";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 213);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(244, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "Пол";
            // 
            // minAge_textbox
            // 
            this.minAge_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.minAge_textbox.Location = new System.Drawing.Point(253, 142);
            this.minAge_textbox.Name = "minAge_textbox";
            this.minAge_textbox.Size = new System.Drawing.Size(586, 22);
            this.minAge_textbox.TabIndex = 11;
            this.minAge_textbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.minAge_textbox_KeyPress);
            // 
            // maxAge_textbox
            // 
            this.maxAge_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.maxAge_textbox.Location = new System.Drawing.Point(253, 176);
            this.maxAge_textbox.Name = "maxAge_textbox";
            this.maxAge_textbox.Size = new System.Drawing.Size(586, 22);
            this.maxAge_textbox.TabIndex = 12;
            this.maxAge_textbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maxAge_textbox_KeyPress);
            // 
            // gender_combobox
            // 
            this.gender_combobox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.gender_combobox.FormattingEnabled = true;
            this.gender_combobox.Location = new System.Drawing.Point(253, 209);
            this.gender_combobox.Name = "gender_combobox";
            this.gender_combobox.Size = new System.Drawing.Size(586, 24);
            this.gender_combobox.TabIndex = 13;
            // 
            // checkedListBox
            // 
            this.checkedListBox.BackColor = System.Drawing.SystemColors.Control;
            this.checkedListBox.FormattingEnabled = true;
            this.checkedListBox.Location = new System.Drawing.Point(6, 246);
            this.checkedListBox.Name = "checkedListBox";
            this.checkedListBox.Size = new System.Drawing.Size(833, 123);
            this.checkedListBox.TabIndex = 15;
            // 
            // create_button
            // 
            this.create_button.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.create_button.Location = new System.Drawing.Point(579, 381);
            this.create_button.Name = "create_button";
            this.create_button.Size = new System.Drawing.Size(260, 40);
            this.create_button.TabIndex = 14;
            this.create_button.Text = "Применить";
            this.create_button.UseVisualStyleBackColor = true;
            this.create_button.Click += new System.EventHandler(this.create_button_Click);
            // 
            // CompetitionEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 433);
            this.Controls.Add(this.checkedListBox);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.create_button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CompetitionEditForm";
            this.Text = "Соревнование";
            this.Load += new System.EventHandler(this.CompetitionEditForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox name_textbox;
        private System.Windows.Forms.DateTimePicker date_datepicker;
        private System.Windows.Forms.ComboBox minSkill_combobox;
        private System.Windows.Forms.ComboBox maxSkill_combobox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox minAge_textbox;
        private System.Windows.Forms.TextBox maxAge_textbox;
        private System.Windows.Forms.Button create_button;
        private System.Windows.Forms.CheckedListBox checkedListBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox gender_combobox;
    }
}