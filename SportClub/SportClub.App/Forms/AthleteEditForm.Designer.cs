﻿namespace SportClub.App.Forms
{
    partial class AthleteEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.firstname_textbox = new System.Windows.Forms.TextBox();
            this.secondname_textbox = new System.Windows.Forms.TextBox();
            this.lastname_textbox = new System.Windows.Forms.TextBox();
            this.gender_dropdown = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.skill_dropdown = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.coach_dropdown = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.bornDate_datepicker = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.rating_textbox = new System.Windows.Forms.TextBox();
            this.create_button = new System.Windows.Forms.Button();
            this.checkedListBox = new System.Windows.Forms.CheckedListBox();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.firstname_textbox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.secondname_textbox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.lastname_textbox, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.gender_dropdown, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.skill_dropdown, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.label7, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.coach_dropdown, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.label8, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.bornDate_datepicker, 1, 6);
            this.tableLayoutPanel.Controls.Add(this.label5, 0, 7);
            this.tableLayoutPanel.Controls.Add(this.rating_textbox, 1, 7);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 8;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49938F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49938F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49938F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49938F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49938F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49938F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.50187F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.50187F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(842, 240);
            this.tableLayoutPanel.TabIndex = 1;
            // 
            // firstname_textbox
            // 
            this.firstname_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstname_textbox.Location = new System.Drawing.Point(253, 3);
            this.firstname_textbox.Name = "firstname_textbox";
            this.firstname_textbox.Size = new System.Drawing.Size(586, 22);
            this.firstname_textbox.TabIndex = 0;
            // 
            // secondname_textbox
            // 
            this.secondname_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.secondname_textbox.Location = new System.Drawing.Point(253, 32);
            this.secondname_textbox.Name = "secondname_textbox";
            this.secondname_textbox.Size = new System.Drawing.Size(586, 22);
            this.secondname_textbox.TabIndex = 1;
            // 
            // lastname_textbox
            // 
            this.lastname_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lastname_textbox.Location = new System.Drawing.Point(253, 61);
            this.lastname_textbox.Name = "lastname_textbox";
            this.lastname_textbox.Size = new System.Drawing.Size(586, 22);
            this.lastname_textbox.TabIndex = 2;
            // 
            // gender_dropdown
            // 
            this.gender_dropdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.gender_dropdown.FormattingEnabled = true;
            this.gender_dropdown.Location = new System.Drawing.Point(253, 90);
            this.gender_dropdown.Name = "gender_dropdown";
            this.gender_dropdown.Size = new System.Drawing.Size(586, 24);
            this.gender_dropdown.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Имя";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(244, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Отчество";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(244, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Фамилия";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(244, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Пол";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(244, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Уровень мастерства";
            // 
            // skill_dropdown
            // 
            this.skill_dropdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.skill_dropdown.FormattingEnabled = true;
            this.skill_dropdown.Location = new System.Drawing.Point(253, 119);
            this.skill_dropdown.Name = "skill_dropdown";
            this.skill_dropdown.Size = new System.Drawing.Size(586, 24);
            this.skill_dropdown.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(244, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Тренер";
            // 
            // coach_dropdown
            // 
            this.coach_dropdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.coach_dropdown.FormattingEnabled = true;
            this.coach_dropdown.Location = new System.Drawing.Point(253, 148);
            this.coach_dropdown.Name = "coach_dropdown";
            this.coach_dropdown.Size = new System.Drawing.Size(586, 24);
            this.coach_dropdown.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(244, 17);
            this.label8.TabIndex = 14;
            this.label8.Text = "Дата рождения";
            // 
            // bornDate_datepicker
            // 
            this.bornDate_datepicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bornDate_datepicker.Location = new System.Drawing.Point(253, 178);
            this.bornDate_datepicker.Name = "bornDate_datepicker";
            this.bornDate_datepicker.Size = new System.Drawing.Size(586, 22);
            this.bornDate_datepicker.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(244, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Рейтинг";
            // 
            // rating_textbox
            // 
            this.rating_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rating_textbox.Location = new System.Drawing.Point(253, 211);
            this.rating_textbox.Name = "rating_textbox";
            this.rating_textbox.Size = new System.Drawing.Size(586, 22);
            this.rating_textbox.TabIndex = 7;
            this.rating_textbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rating_textbox_KeyPress);
            // 
            // create_button
            // 
            this.create_button.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.create_button.Location = new System.Drawing.Point(579, 381);
            this.create_button.Name = "create_button";
            this.create_button.Size = new System.Drawing.Size(260, 40);
            this.create_button.TabIndex = 8;
            this.create_button.Text = "Применить";
            this.create_button.UseVisualStyleBackColor = true;
            this.create_button.Click += new System.EventHandler(this.create_button_Click);
            // 
            // checkedListBox
            // 
            this.checkedListBox.BackColor = System.Drawing.SystemColors.Control;
            this.checkedListBox.FormattingEnabled = true;
            this.checkedListBox.Location = new System.Drawing.Point(6, 246);
            this.checkedListBox.Name = "checkedListBox";
            this.checkedListBox.Size = new System.Drawing.Size(833, 123);
            this.checkedListBox.TabIndex = 9;
            // 
            // AthleteEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 433);
            this.Controls.Add(this.checkedListBox);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.create_button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AthleteEditForm";
            this.Text = "Спортсмен";
            this.Load += new System.EventHandler(this.AthleteEditForm_Load);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TextBox firstname_textbox;
        private System.Windows.Forms.TextBox secondname_textbox;
        private System.Windows.Forms.TextBox lastname_textbox;
        private System.Windows.Forms.ComboBox gender_dropdown;
        private System.Windows.Forms.TextBox rating_textbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox skill_dropdown;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox coach_dropdown;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker bornDate_datepicker;
        private System.Windows.Forms.Button create_button;
        private System.Windows.Forms.CheckedListBox checkedListBox;
    }
}