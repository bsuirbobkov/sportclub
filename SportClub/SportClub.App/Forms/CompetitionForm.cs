﻿using System.Windows.Forms;
using SportClub.Logic.Entities;
using SportClub.Logic.Models;
using SportClub.Logic.Services;

namespace SportClub.App.Forms
{
    public partial class CompetitionForm : Form
    {
        private readonly CompetitionService competitionService = new CompetitionService();
        private readonly SkillService skillService = new SkillService();

        public CompetitionForm()
        {
            InitializeComponent();
        }

        private void CompetitionForm_Load(object sender, System.EventArgs e)
        {
            ReloadContent();
            SetStaticContent();
        }

        private async void ReloadContent()
        {
            var dataSource = await DataSourceHelper.Create(() => competitionService.GetAll());
            DataGridHelper.Set(dataGridView, dataSource);
            DataGridHelper.HideColumns(dataGridView,
                nameof(CompetitionModel.Id),
                nameof(CompetitionModel.MinSkillId),
                nameof(CompetitionModel.MaxSkillId),
                nameof(CompetitionModel.GenderId)
            );
        }

        private void SetStaticContent()
        {
            var skills = skillService.GetAll();
            FormHelper.SetComboBox(
                requiredSkill,
                skills,
                null,
                nameof(SkillEntity.Id),
                nameof(SkillEntity.Name)
            );
        }

        private FilterModel GetFilter()
        {
            var minAge = string.IsNullOrWhiteSpace(requiredMinAge.Text)
                ? 0
                : int.Parse(requiredMinAge.Text);

            var maxAge = string.IsNullOrWhiteSpace(requiredMaxAge.Text)
                ? 0
                : int.Parse(requiredMaxAge.Text);

            return new FilterModel
            {
                SkillId = (int)requiredSkill.SelectedValue,
                MinAge = minAge,
                MaxAge = maxAge
            };
        }

        private void create_button_Click(object sender, System.EventArgs e)
        {
            var form = new CompetitionEditForm(null, GetFilter());
            form.ShowDialog();
            ReloadContent();
        }

        private void edit_button_Click(object sender, System.EventArgs e)
        {
            if (dataGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите запись");
                return;
            }
            var model = DataGridHelper.Get<CompetitionModel>(dataGridView);
            var editForm = new CompetitionEditForm(model, GetFilter());
            editForm.ShowDialog();
            ReloadContent();
        }

        private void delete_button_Click(object sender, System.EventArgs e)
        {
            if (dataGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите запись");
                return;
            }
            var model = DataGridHelper.Get<CompetitionModel>(dataGridView);
            competitionService.Delete(model.Id);
            ReloadContent();
        }

        private void requiredMinAge_KeyPress(object sender, KeyPressEventArgs e)
        {
            FormHelper.NumberOnly(sender, e);
        }

        private void requiredMaxAge_KeyPress(object sender, KeyPressEventArgs e)
        {
            FormHelper.NumberOnly(sender, e);
        }

        private void requiredAllowTraumas_CheckedChanged(object sender, System.EventArgs e)
        {

        }
    }
}