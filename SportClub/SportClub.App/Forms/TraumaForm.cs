﻿using System.Windows.Forms;
using SportClub.Logic.Entities;
using SportClub.Logic.Services;

namespace SportClub.App.Forms
{
    public partial class TraumaForm : Form
    {
        private readonly TraumaService traumaService = new TraumaService();

        public TraumaForm()
        {
            InitializeComponent();
        }

        private void TraumaForm_Load(object sender, System.EventArgs e)
        {
            ReloadContent();
        }

        private async void ReloadContent()
        {
            var dataSource = await DataSourceHelper.Create(() => traumaService.GetAll());
            DataGridHelper.Set(dataGridView, dataSource);
            DataGridHelper.HideColumns(dataGridView,
                nameof(TraumaEntity.Id)
            );
        }

        private void create_button_Click(object sender, System.EventArgs e)
        {
            var form = new TraumaEditForm(null);
            form.ShowDialog();
            ReloadContent();
        }

        private void edit_button_Click(object sender, System.EventArgs e)
        {
            if (dataGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите запись");
                return;
            }
            var item = DataGridHelper.Get<TraumaEntity>(dataGridView);
            var form = new TraumaEditForm(item);
            form.ShowDialog();
            ReloadContent();
        }

        private void delete_button_Click(object sender, System.EventArgs e)
        {
            if (dataGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите запись");
                return;
            }
            var item = DataGridHelper.Get<TraumaEntity>(dataGridView);
            traumaService.Delete(item.Id);
            ReloadContent();
        }
    }
}
