﻿using System.Windows.Forms;
using SportClub.Logic.Entities;
using SportClub.Logic.Services;

namespace SportClub.App.Forms
{
    public partial class TraumaEditForm : Form
    {
        private readonly TraumaService traumaService = new TraumaService();
        private readonly bool editMode;
        private readonly TraumaEntity model;

        public TraumaEditForm(TraumaEntity model)
        {
            editMode = model != null;
            this.model = model;
            InitializeComponent();
        }

        private void TraumaEditForm_Load(object sender, System.EventArgs e)
        {
            textBox.Text = model?.Name;
        }

        private void add_button_Click(object sender, System.EventArgs e)
        {
            if (textBox.Text.Length == 0)
            {
                MessageBox.Show("Введите значение");
                return;
            }
            if (editMode)
            {
                traumaService.Update(model.Id, textBox.Text);
            }
            else
            {
                traumaService.Create(textBox.Text);
            }
            Close();
        }
    }
}
