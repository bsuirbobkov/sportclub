﻿using System.Windows.Forms;
using SportClub.Logic.Entities;
using SportClub.Logic.Models;
using SportClub.Logic.Services;

namespace SportClub.App.Forms
{
    public partial class CoachEditForm : Form
    {
        private readonly CoachService coachService = new CoachService();
        private readonly GenderService genderService = new GenderService();

        private readonly bool editMode;
        private readonly CoachModel model;

        public CoachEditForm(CoachModel model)
        {
            editMode = model != null;
            this.model = model;
            InitializeComponent();
        }

        private void CoachEditForm_Load(object sender, System.EventArgs e)
        {
            SetModel();
        }

        private void create_button_Click(object sender, System.EventArgs e)
        {
            if (!IsValid())
            {
                MessageBox.Show("Введите значение");
                return;
            }
            if (editMode)
            {
                coachService.Update(GetModel());
            }
            else
            {
                coachService.Create(GetModel());
            }
            Close();
        }

        private void SetModel()
        {
            var genders = genderService.GetAll();
            firstname_textbox.Text = model?.FirstName;
            secondname_textbox.Text = model?.SecondName;
            lastname_textbox.Text = model?.LastName;
            rating_textbox.Text = model?.Rating.ToString();

            FormHelper.SetComboBox(
                gender_dropdown,
                genders,
                model?.GenderId,
                nameof(GenderEntity.Id),
                nameof(GenderEntity.Name)
            );
        }

        private CoachModel GetModel()
        {
            var gender = (GenderEntity)gender_dropdown.SelectedItem;
            return new CoachModel
            {
                Id = model?.Id ?? 0,
                FirstName = firstname_textbox.Text,
                SecondName = secondname_textbox.Text,
                LastName = lastname_textbox.Text,
                GenderId = gender.Id,
                Rating = int.Parse(rating_textbox.Text)
            };
        }

        private bool IsValid()
        {
            if (firstname_textbox.Text.Length == 0)
            {
                return false;
            }
            if (secondname_textbox.Text.Length == 0)
            {
                return false;
            }
            if (lastname_textbox.Text.Length == 0)
            {
                return false;
            }
            if (gender_dropdown.SelectedItem == null)
            {
                return false;
            }
            if (rating_textbox.Text.Length == 0)
            {
                return false;
            }
            return true;
        }

        private void rating_textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            FormHelper.NumberOnly(sender, e);
        }
    }
}
