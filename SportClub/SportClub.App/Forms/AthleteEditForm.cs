﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SportClub.Logic.Entities;
using SportClub.Logic.Models;
using SportClub.Logic.Services;

namespace SportClub.App.Forms
{
    public partial class AthleteEditForm : Form
    {
        private readonly AthleteService athleteService = new AthleteService();
        private readonly CoachService coachService = new CoachService();
        private readonly GenderService genderService = new GenderService();
        private readonly SkillService skillService = new SkillService();
        private readonly TraumaService traumaService = new TraumaService();

        private readonly bool editMode;
        private readonly AthleteModel model;

        public AthleteEditForm(AthleteModel model)
        {
            editMode = model != null;
            this.model = model;
            InitializeComponent();
        }

        private void AthleteEditForm_Load(object sender, EventArgs e)
        {
            SetModel();
        }

        private void create_button_Click(object sender, EventArgs e)
        {
            if (!IsValid())
            {
                MessageBox.Show("Введите значение");
                return;
            }
            if (editMode)
            {
                athleteService.Update(GetModel(), GetCheckedTraumas());
            }
            else
            {
                athleteService.Create(GetModel(), GetCheckedTraumas());
            }
            Close();
        }

        private void SetModel()
        {
            var genders = genderService.GetAll();
            var skills = skillService.GetAll();
            var coaches = coachService.GetAllEntities();
            var traumas = traumaService.GetForAthlete(model?.Id ?? 0);

            firstname_textbox.Text = model?.FirstName;
            secondname_textbox.Text = model?.SecondName;
            lastname_textbox.Text = model?.LastName;
            bornDate_datepicker.Value = model?.BornDate ?? DateTime.Now;
            rating_textbox.Text = model?.Rating.ToString();

            FormHelper.SetComboBox(
                gender_dropdown,
                genders,
                model?.GenderId,
                nameof(GenderEntity.Id),
                nameof(GenderEntity.Name)
            );

            FormHelper.SetComboBox(
                skill_dropdown,
                skills,
                model?.SkillId,
                nameof(SkillEntity.Id),
                nameof(SkillEntity.Name)
            );

            FormHelper.SetComboBox(
                coach_dropdown,
                coaches,
                model?.CoachId,
                nameof(CoachEntity.Id),
                nameof(CoachEntity.LastName)
            );

            FormHelper.SetCheckList(checkedListBox, traumas);
        }

        private AthleteModel GetModel()
        {
            var gender = (GenderEntity)gender_dropdown.SelectedItem;
            var skill = (SkillEntity)skill_dropdown.SelectedItem;
            var coach = coach_dropdown.SelectedItem as CoachEntity;

            return new AthleteModel
            {
                Id = model?.Id ?? 0,
                FirstName = firstname_textbox.Text,
                SecondName = secondname_textbox.Text,
                LastName = lastname_textbox.Text,
                GenderId = gender.Id,
                SkillId = skill.Id,
                CoachId = coach?.Id,
                BornDate = bornDate_datepicker.Value,
                Rating = int.Parse(rating_textbox.Text)
            };
        }

        private IReadOnlyCollection<CheckItemModel> GetCheckedTraumas()
        {
            var result = new List<CheckItemModel>();
            foreach (var item in checkedListBox.CheckedItems)
            {
                result.Add((CheckItemModel)item);
            }
            return result;
        }

        private bool IsValid()
        {
            if (firstname_textbox.Text.Length == 0)
            {
                return false;
            }
            if (secondname_textbox.Text.Length == 0)
            {
                return false;
            }
            if (lastname_textbox.Text.Length == 0)
            {
                return false;
            }
            if (gender_dropdown.SelectedItem == null)
            {
                return false;
            }
            if (skill_dropdown.SelectedItem == null)
            {
                return false;
            }
            if (rating_textbox.Text.Length == 0)
            {
                return false;
            }
            return true;
        }

        private void rating_textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            FormHelper.NumberOnly(sender, e);
        }
    }
}