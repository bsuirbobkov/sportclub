﻿using System.Windows.Forms;
using SportClub.Logic.Models;
using SportClub.Logic.Services;

namespace SportClub.App.Forms
{
    public partial class CoachForm : Form
    {
        private readonly CoachService coachService = new CoachService();

        public CoachForm()
        {
            InitializeComponent();
        }

        private void CoachForm_Load(object sender, System.EventArgs e)
        {
            ReloadContent();
        }

        private async void ReloadContent()
        {
            var dataSource = await DataSourceHelper.Create(() => coachService.GetAll());
            DataGridHelper.Set(dataGridView, dataSource);
            DataGridHelper.HideColumns(dataGridView,
                nameof(CoachModel.Id),
                nameof(CoachModel.GenderId)
            );
        }

        private void create_button_Click(object sender, System.EventArgs e)
        {
            var editForm = new CoachEditForm(null);
            editForm.ShowDialog();
            ReloadContent();
        }

        private void edit_button_Click(object sender, System.EventArgs e)
        {
            if (dataGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите запись");
                return;
            }
            var model = DataGridHelper.Get<CoachModel>(dataGridView);
            var editForm = new CoachEditForm(model);
            editForm.ShowDialog();
            ReloadContent();
        }

        private void delete_button_Click(object sender, System.EventArgs e)
        {
            if (dataGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите запись");
                return;
            }
            var model = DataGridHelper.Get<CoachModel>(dataGridView);
            coachService.Delete(model.Id);
            ReloadContent();
        }
    }
}
