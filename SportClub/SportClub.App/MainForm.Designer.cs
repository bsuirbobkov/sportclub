﻿namespace SportClub.App
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.manage_athletes = new System.Windows.Forms.Button();
            this.manage_coaches = new System.Windows.Forms.Button();
            this.manage_competitions = new System.Windows.Forms.Button();
            this.manage_traumas = new System.Windows.Forms.Button();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.manage_athletes, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.manage_coaches, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.manage_competitions, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.manage_traumas, 0, 3);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.Padding = new System.Windows.Forms.Padding(11, 0, 11, 0);
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(212, 254);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // manage_athletes
            // 
            this.manage_athletes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.manage_athletes.Location = new System.Drawing.Point(13, 17);
            this.manage_athletes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.manage_athletes.Name = "manage_athletes";
            this.manage_athletes.Size = new System.Drawing.Size(186, 28);
            this.manage_athletes.TabIndex = 0;
            this.manage_athletes.Text = "Спортсмены";
            this.manage_athletes.UseVisualStyleBackColor = true;
            this.manage_athletes.Click += new System.EventHandler(this.manage_athletes_Click);
            // 
            // manage_coaches
            // 
            this.manage_coaches.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.manage_coaches.Location = new System.Drawing.Point(13, 80);
            this.manage_coaches.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.manage_coaches.Name = "manage_coaches";
            this.manage_coaches.Size = new System.Drawing.Size(186, 28);
            this.manage_coaches.TabIndex = 1;
            this.manage_coaches.Text = "Тренеры";
            this.manage_coaches.UseVisualStyleBackColor = true;
            this.manage_coaches.Click += new System.EventHandler(this.manage_coaches_Click);
            // 
            // manage_competitions
            // 
            this.manage_competitions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.manage_competitions.Location = new System.Drawing.Point(13, 143);
            this.manage_competitions.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.manage_competitions.Name = "manage_competitions";
            this.manage_competitions.Size = new System.Drawing.Size(186, 28);
            this.manage_competitions.TabIndex = 2;
            this.manage_competitions.Text = "Соревнования";
            this.manage_competitions.UseVisualStyleBackColor = true;
            this.manage_competitions.Click += new System.EventHandler(this.manage_competitions_Click);
            // 
            // manage_traumas
            // 
            this.manage_traumas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.manage_traumas.Location = new System.Drawing.Point(13, 207);
            this.manage_traumas.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.manage_traumas.Name = "manage_traumas";
            this.manage_traumas.Size = new System.Drawing.Size(186, 28);
            this.manage_traumas.TabIndex = 5;
            this.manage_traumas.Text = "Травмы";
            this.manage_traumas.UseVisualStyleBackColor = true;
            this.manage_traumas.Click += new System.EventHandler(this.manage_traumas_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(212, 254);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Главная форма";
            this.Load += new System.EventHandler(this.FormTest_Load);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Button manage_athletes;
        private System.Windows.Forms.Button manage_coaches;
        private System.Windows.Forms.Button manage_competitions;
        private System.Windows.Forms.Button manage_traumas;
    }
}

