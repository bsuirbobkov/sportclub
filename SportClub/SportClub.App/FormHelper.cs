﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SportClub.Logic.Models;

namespace SportClub.App
{
    public static class FormHelper
    {
        public static void NumberOnly(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && (((TextBox)sender).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        public static void SetComboBox<T>(
            ComboBox component,
            IReadOnlyCollection<T> items,
            object value,
            string valueName,
            string textName)
        {
            component.DataSource = items.ToList();
            component.DropDownStyle = ComboBoxStyle.DropDownList;
            component.ValueMember = valueName;
            component.DisplayMember = textName;

            if (value != null)
            {
                component.SelectedValue = value;
            }
        }

        public static void SetCheckList(
            CheckedListBox checkList,
            IReadOnlyCollection<CheckItemModel> items)
        {
            checkList.ValueMember = nameof(CheckItemModel.Id);
            checkList.DisplayMember = nameof(CheckItemModel.Name);
            foreach (var checkItem in items)
            {
                checkList.Items.Add(checkItem);
                checkList.SetItemChecked(
                    checkList.Items.Count - 1,
                    checkItem.Checked
                );
            }
        }
    }
}