﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SportClub.App
{
    public static class DataSourceHelper
    {
        public static Task<BindingSource> Create<T>(Func<IReadOnlyCollection<T>> func)
        {
            return Task.Run(() =>
            {
                var items = func();
                var source = new BindingSource { DataSource = items };
                return source;
            });
        }
    }
}