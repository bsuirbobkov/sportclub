﻿CREATE TABLE [dbo].[AthleteToCompetition]
(
    [AthleteId] INT NOT NULL,
    [CompetitionId] INT NOT NULL,

    PRIMARY KEY (AthleteId, CompetitionId),
    FOREIGN KEY ([AthleteId]) REFERENCES [Athlete]([Id]) ON DELETE CASCADE,
    FOREIGN KEY ([CompetitionId]) REFERENCES [Competition]([Id]) ON DELETE CASCADE
)
