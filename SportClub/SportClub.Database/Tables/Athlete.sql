﻿CREATE TABLE [dbo].[Athlete]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [FirstName] NVARCHAR(50) NULL, 
    [SecondName] NVARCHAR(50) NULL, 
    [LastName] NVARCHAR(50) NULL, 
    [GenderId] INT NOT NULL,
    [SkillId] INT NOT NULL,
    [CoachId] INT NULL,
    [BornDate] DATETIME NOT NULL, 
    [Rating] INT NOT NULL,

    FOREIGN KEY ([GenderId]) REFERENCES [Gender]([Id]),
    FOREIGN KEY ([SkillId]) REFERENCES [Skill]([Id]),
    FOREIGN KEY ([CoachId]) REFERENCES [Coach]([Id]) ON DELETE SET NULL,
    CONSTRAINT CHK_BornDate_LessThanNow CHECK ([BornDate] <= GETDATE())
)
