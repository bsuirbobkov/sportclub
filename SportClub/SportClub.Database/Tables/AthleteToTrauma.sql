﻿CREATE TABLE [dbo].[AthleteToTrauma]
(
    [AthleteId] INT NOT NULL,
    [TraumaId] INT NOT NULL,

    PRIMARY KEY (AthleteId, TraumaId),
    FOREIGN KEY ([AthleteId]) REFERENCES [Athlete]([Id]) ON DELETE CASCADE,
    FOREIGN KEY ([TraumaId]) REFERENCES [Trauma]([Id]) ON DELETE CASCADE
)
