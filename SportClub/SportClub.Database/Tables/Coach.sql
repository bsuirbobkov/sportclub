﻿CREATE TABLE [dbo].[Coach]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [FirstName] NVARCHAR(50) NULL,
    [SecondName] NVARCHAR(50) NULL,
    [LastName] NVARCHAR(50) NULL,
    [GenderId] INT NOT NULL,
    [Rating] INT NOT NULL,

    FOREIGN KEY ([GenderId]) REFERENCES [Gender]([Id])
)
