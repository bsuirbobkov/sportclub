﻿CREATE TABLE [dbo].[Competition]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [Name] NVARCHAR(50) NULL,
    [Date] DATETIME NULL,
    [MinSkillId] INT NOT NULL,
    [MaxSkillId] INT NOT NULL,
    [MinAge] INT NOT NULL,
    [MaxAge] INT NOT NULL,
    [GenderId] INT NOT NULL,

    FOREIGN KEY ([MinSkillId]) REFERENCES [Skill]([Id]),
    FOREIGN KEY ([MaxSkillId]) REFERENCES [Skill]([Id]),
    FOREIGN KEY ([GenderId]) REFERENCES [Gender]([Id]),
    CONSTRAINT CHK_AgeRange CHECK ([MinAge] <= [MaxAge]),
    CONSTRAINT CHK_MinAge_ValidValue CHECK ([MinAge] BETWEEN 0 AND 120),
    CONSTRAINT CHK_MaxAge_ValidValue CHECK ([MaxAge] BETWEEN 0 AND 120)
)
