﻿CREATE PROCEDURE [dbo].[Coach_Update]
    @id INT,
    @firstName nvarchar(50),
    @secondName nvarchar(50),
    @lastName nvarchar(50),
    @genderId INT,
    @rating INT
AS
    UPDATE [dbo].[Coach]
    SET
        [FirstName] = @firstName,
        [SecondName] = @secondName,
        [LastName] = @lastName,
        [GenderId] = @genderId,
        [Rating] = @rating
    WHERE
        [Id] = @id
RETURN 0
