﻿CREATE PROCEDURE [dbo].[Coach_Insert]
    @firstName nvarchar(50),
    @secondName nvarchar(50),
    @lastName nvarchar(50),
    @genderId INT,
    @rating INT
AS
    INSERT INTO [dbo].[Coach]
    (
        [FirstName],
        [SecondName],
        [LastName],
        [GenderId],
        [Rating]
    )
    VALUES
    (
        @firstName,
        @secondName,
        @lastName,
        @genderId,
        @rating
    )
RETURN SELECT TOP(1) SCOPE_IDENTITY()
