﻿CREATE PROCEDURE [dbo].[Coach_DeleteById]
    @id int = 0
AS
    DELETE FROM [dbo].[Coach]
    WHERE Id = @id
RETURN 0
