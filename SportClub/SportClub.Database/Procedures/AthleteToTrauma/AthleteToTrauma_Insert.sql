﻿CREATE PROCEDURE [dbo].[AthleteToTrauma_Insert]
    @athleteId INT,
    @traumaId INT
AS
    INSERT INTO [dbo].[AthleteToTrauma]
    (
        AthleteId,
        TraumaId
    )
    VALUES
    (
        @athleteId,
        @traumaId
    )
RETURN 0
