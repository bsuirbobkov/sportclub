﻿CREATE PROCEDURE [dbo].[AthleteToTrauma_DeleteByAthleteId]
    @athleteId int
AS
    DELETE FROM [dbo].[AthleteToTrauma]
    WHERE AthleteId = @athleteId
RETURN 0
