﻿CREATE PROCEDURE [dbo].[Trauma_DeleteById]
    @id int = 0
AS
    DELETE FROM [dbo].[Trauma]
    WHERE Id = @id
RETURN 0
