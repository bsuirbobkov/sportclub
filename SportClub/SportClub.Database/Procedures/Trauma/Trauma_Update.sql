﻿CREATE PROCEDURE [dbo].[Trauma_Update]
    @id INT,
    @name NVARCHAR(50)
AS
    UPDATE [dbo].Trauma
    SET
        [Name] = @name
    WHERE
        [Id] = @id
RETURN 0
