﻿CREATE PROCEDURE [dbo].[Competition_Insert]
    @name NVARCHAR(50),
    @date DATETIME,
    @minSkillId INT,
    @maxSkillId INT,
    @minAge INT,
    @maxAge INT,
    @genderId INT
AS
    INSERT INTO [dbo].[Competition]
    (
        [Name],
        [Date],
        [MinSkillId],
        [MaxSkillId],
        [MinAge],
        [MaxAge],
        [GenderId]
    )
    VALUES
    (
        @name,
        @date,
        @minSkillId,
        @maxSkillId,
        @minAge,
        @maxAge,
        @genderId
    );

    SELECT SCOPE_IDENTITY();
RETURN 0
