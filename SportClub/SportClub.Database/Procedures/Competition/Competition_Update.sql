﻿CREATE PROCEDURE [dbo].[Competition_Update]
    @id INT,
    @name NVARCHAR(50),
    @date DATETIME,
    @minSkillId INT,
    @maxSkillId INT,
    @minAge INT,
    @maxAge INT,
    @genderId INT
AS
    UPDATE [dbo].[Competition]
    SET
        [Name] = @name,
        [Date] = @date,
        [MinSkillId] = @minSkillId,
        [MaxSkillId] = @maxSkillId,
        [MinAge] = @minAge,
        [MaxAge] = @maxAge,
        [GenderId] = @genderId
    WHERE
        [Id] = @id
RETURN 0
