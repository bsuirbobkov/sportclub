﻿CREATE PROCEDURE [dbo].[Competition_DeleteById]
    @id int = 0
AS
    DELETE FROM [dbo].[Competition]
    WHERE Id = @id
RETURN 0
