﻿CREATE PROCEDURE [dbo].[Athlete_Insert]
    @firstName nvarchar(50),
    @secondName nvarchar(50),
    @lastName nvarchar(50),
    @genderId INT,
    @skillId INT,
    @coachId INT,
    @rating INT,
    @bornDate Datetime
AS
    INSERT INTO [dbo].[Athlete]
    (
        [FirstName],
        [SecondName],
        [LastName],
        [GenderId],
        [SkillId],
        [CoachId],
        [Rating],
        [BornDate]
    )
    VALUES
    (
        @firstName,
        @secondName,
        @lastName,
        @genderId,
        @skillId,
        @coachId,
        @rating,
        @bornDate
    );

    SELECT SCOPE_IDENTITY();
RETURN 0;
