﻿CREATE PROCEDURE [dbo].[Athlete_DeleteById]
    @id int = 0
AS
    DELETE FROM [dbo].[Athlete]
    WHERE Id = @id
RETURN 0
