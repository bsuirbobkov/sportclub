﻿CREATE PROCEDURE [dbo].[Athlete_Filter]
    @skillId int,
    @minAge int,
    @maxAge int
AS
    SELECT * FROM [dbo].[Athlete]
    WHERE
        Athlete.SkillId = @skillId
        AND Athlete.Id NOT IN (
            SELECT
                AthleteId 
            FROM
                [dbo].[AthleteToTrauma]
            WHERE
                AthleteToTrauma.AthleteId = Athlete.Id
        )
        AND DATEDIFF(YEAR, Athlete.BornDate, GETDATE()) >= @minAge
        AND DATEDIFF(YEAR, Athlete.BornDate, GETDATE()) <= @maxAge
RETURN 0
