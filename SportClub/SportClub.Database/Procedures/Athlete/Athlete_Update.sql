﻿CREATE PROCEDURE [dbo].[Athlete_Update]
    @id INT,
    @firstName nvarchar(50),
    @secondName nvarchar(50),
    @lastName nvarchar(50),
    @genderId INT,
    @skillId INT,
    @coachId INT,
    @rating INT,
    @bornDate Datetime
AS
    UPDATE [dbo].[Athlete]
    SET
        [FirstName] = @firstName,
        [SecondName] = @secondName,
        [LastName] = @lastName,
        [GenderId] = @genderId,
        [SkillId] = @skillId,
        [CoachId] = @coachId,
        [Rating] = @rating,
        [BornDate] = @bornDate
    WHERE
        [Id] = @id
RETURN 0
