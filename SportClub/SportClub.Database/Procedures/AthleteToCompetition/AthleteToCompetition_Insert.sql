﻿CREATE PROCEDURE [dbo].[AthleteToCompetition_Insert]
    @athleteId int,
    @competitionId int
AS
    INSERT INTO [dbo].[AthleteToCompetition]
    (
        AthleteId,
        CompetitionId
    )
    VALUES
    (
        @athleteId,
        @competitionId
    )
RETURN 0
