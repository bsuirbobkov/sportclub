﻿CREATE PROCEDURE [dbo].[AthleteToCompetition_DeleteByCompetitionId]
    @competitionId int
AS
    DELETE FROM [dbo].[AthleteToCompetition]
    WHERE CompetitionId = @competitionId
RETURN 0
