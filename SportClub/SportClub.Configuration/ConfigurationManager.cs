﻿using System;
using Microsoft.Extensions.Configuration;

namespace SportClub.Configuration
{
    public static class ConfigurationManager
    {
        private static IConfigurationRoot _configuration;

        public static void BuildConfiguration()
        {
            _configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
        }

        public static string ConnectionString => _configuration.GetConnectionString("DefaultConnection");
    }
}
