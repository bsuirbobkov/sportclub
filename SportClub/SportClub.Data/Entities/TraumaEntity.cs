﻿namespace SportClub.Logic.Entities
{
    public class TraumaEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}