﻿using System;

namespace SportClub.Logic.Entities
{
    public class CompetitionEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public int MinSkillId { get; set; }
        public int MaxSkillId { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int GenderId { get; set; }
    }
}