﻿namespace SportClub.Logic.Entities
{
    public class CoachEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public int GenderId { get; set; }
        public int Rating { get; set; }
    }
}