﻿namespace SportClub.Logic.Entities
{
    public class AthleteToCompetitionEntity
    {
        public int AthleteId { get; set; }
        public int CompetitionId { get; set; }
    }
}