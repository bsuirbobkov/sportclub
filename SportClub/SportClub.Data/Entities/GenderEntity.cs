﻿namespace SportClub.Logic.Entities
{
    public class GenderEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}