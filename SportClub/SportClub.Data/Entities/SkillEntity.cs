﻿namespace SportClub.Logic.Entities
{
    public class SkillEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}