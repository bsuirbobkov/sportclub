﻿using System.Collections.Generic;
using System.Linq;
using SportClub.Logic.Entities;
using SportClub.Logic.Models;

namespace SportClub.Logic.Services
{
    public class CoachService : BaseService<CoachModel>
    {
        public override IReadOnlyCollection<CoachModel> GetAll()
        {
            var genders = Repository.QueryMany<GenderEntity>(DbProcedure.Gender.Select);
            var coaches = Repository.QueryMany<CoachEntity>(DbProcedure.Coach.Select);
            var items = coaches.OrderByDescending(x => x.Rating).Select(x =>
            {
                var gender = genders.First(c => c.Id == x.GenderId);
                return new CoachModel
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    SecondName = x.SecondName,
                    LastName = x.LastName,
                    GenderId = gender.Id,
                    GenderName = gender.Name,
                    Rating = x.Rating
                };
            });

            return items.ToList();
        }

        public IReadOnlyCollection<CoachEntity> GetAllEntities()
        {
            return Repository.QueryMany<CoachEntity>(DbProcedure.Coach.Select);
        }

        public void Delete(int id)
        {
            var data = new { id = id };
            Repository.Execute(DbProcedure.Coach.DeleteById, data);
        }

        public void Update(CoachModel model)
        {
            var data = new
            {
                model.Id,
                model.FirstName,
                model.SecondName,
                model.LastName,
                model.GenderId,
                model.Rating
            };
            Repository.Execute(DbProcedure.Coach.Update, data);
        }

        public void Create(CoachModel model)
        {
            var data = new
            {
                model.FirstName,
                model.SecondName,
                model.LastName,
                model.GenderId,
                model.Rating
            };
            Repository.Execute(DbProcedure.Coach.Insert, data);
        }
    }
}