﻿using System.Collections.Generic;
using System.Linq;
using SportClub.Logic.Entities;
using SportClub.Logic.Models;

namespace SportClub.Logic.Services
{
    public class AthleteService : BaseService<AthleteModel>
    {
        public override IReadOnlyCollection<AthleteModel> GetAll()
        {
            var genders = Repository.QueryMany<GenderEntity>(DbProcedure.Gender.Select);
            var coaches = Repository.QueryMany<CoachEntity>(DbProcedure.Coach.Select);
            var skills = Repository.QueryMany<SkillEntity>(DbProcedure.Skill.Select);
            var athletes = Repository.QueryMany<AthleteEntity>(DbProcedure.Athlete.Select);

            var items = athletes.OrderByDescending(x => x.Rating).Select(x =>
            {
                var genderEntry = genders.First(c => c.Id == x.GenderId);
                var skillEntry = skills.First(c => c.Id == x.SkillId);
                var coachEntry = coaches.FirstOrDefault(c => c.Id == x.CoachId);

                return new AthleteModel
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    SecondName = x.SecondName,
                    LastName = x.LastName,
                    GenderId = genderEntry.Id,
                    GenderName = genderEntry.Name,
                    SkillId = skillEntry.Id,
                    SkillName = skillEntry.Name,
                    CoachId = coachEntry?.Id,
                    CoachName = coachEntry?.LastName,
                    Rating = x.Rating,
                    BornDate = x.BornDate
                };
            });
            return items.ToList();
        }

        public void Delete(int id)
        {
            var data = new { id = id };
            Repository.Execute(DbProcedure.Athlete.DeleteById, data);
        }

        public void Update(AthleteModel model, IReadOnlyCollection<CheckItemModel> traumas)
        {
            var data = new
            {
                model.Id,
                model.FirstName,
                model.SecondName,
                model.LastName,
                model.GenderId,
                model.SkillId,
                model.CoachId,
                model.BornDate,
                model.Rating
            };
            Repository.Execute(DbProcedure.Athlete.Update, data);
            Repository.Execute(DbProcedure.AthleteToTrauma.DeleteByAthleteId, new { athleteId = model.Id });
            InsertTraumas(model.Id, traumas);
        }

        public void Create(AthleteModel model, IReadOnlyCollection<CheckItemModel> traumas)
        {
            var data = new
            {
                model.FirstName,
                model.SecondName,
                model.LastName,
                model.GenderId,
                model.SkillId,
                model.CoachId,
                model.BornDate,
                model.Rating
            };
            var athleteId = Repository.QuerySingle<int>(DbProcedure.Athlete.Insert, data);
            InsertTraumas(athleteId, traumas);
        }

        private void InsertTraumas(int athleteId, IEnumerable<CheckItemModel> traumas)
        {
            foreach (var trauma in traumas)
            {
                Repository.Execute(
                    DbProcedure.AthleteToTrauma.Insert,
                    new AthleteToTraumaEntity { AthleteId = athleteId, TraumaId = trauma.Id }
                );
            }
        }

        public IReadOnlyCollection<CheckItemModel> GetForCompetition(int competitionId, FilterModel filter)
        {
            var data = new { id = competitionId };
            var filterDate = new
            {
                skillId = filter.SkillId,
                minAge = filter.MinAge,
                maxAge = filter.MaxAge
            };
            var entries = Repository.QueryMany<AthleteToTraumaEntity>(DbProcedure.AthleteToCompetition.GetByCompetitionId, data);
            var athleteIds = entries.Select(x => x.AthleteId);
            var athletes = Repository.QueryMany<AthleteEntity>(DbProcedure.Athlete.Filter, filterDate);
            var items = athletes.Select(x => new CheckItemModel
            {
                Id = x.Id,
                Name = $"{x.FirstName} {x.LastName}",
                Checked = athleteIds.Contains(x.Id)
            });
            return items.ToList();
        }
    }
}