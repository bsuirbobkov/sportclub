﻿using System.Collections.Generic;
using SportClub.Logic.Entities;

namespace SportClub.Logic.Services
{
    public class SkillService : BaseService<SkillEntity>
    {
        public override IReadOnlyCollection<SkillEntity> GetAll()
        {
            return Repository.QueryMany<SkillEntity>(DbProcedure.Skill.Select);
        }
    }
}