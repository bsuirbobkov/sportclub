﻿using System.Collections.Generic;
using System.Linq;
using SportClub.Logic.Entities;
using SportClub.Logic.Models;

namespace SportClub.Logic.Services
{
    public class TraumaService : BaseService<TraumaEntity>
    {
        public override IReadOnlyCollection<TraumaEntity> GetAll()
        {
            return Repository.QueryMany<TraumaEntity>(DbProcedure.Trauma.Select);
        }

        public void Delete(int id)
        {
            var data = new { id = id };
            Repository.Execute(DbProcedure.Trauma.DeleteById, data);
        }

        public void Create(string name)
        {
            var data = new { name = name };
            Repository.Execute(DbProcedure.Trauma.Insert, data);
        }

        public void Update(int entityId, string name)
        {
            var data = new TraumaEntity { Id = entityId, Name = name };
            Repository.Execute(DbProcedure.Trauma.Update, data);
        }

        public IReadOnlyCollection<CheckItemModel> GetForAthlete(int athleteId)
        {
            var data = new { id = athleteId };
            var entries = Repository.QueryMany<AthleteToTraumaEntity>(DbProcedure.AthleteToTrauma.GetByAthleteId, data);
            var traumaIds = entries.Select(x => x.TraumaId);
            var traumas = Repository.QueryMany<TraumaEntity>(DbProcedure.Trauma.Select);
            var items = traumas.Select(x => new CheckItemModel
            {
                Id = x.Id,
                Name = x.Name,
                Checked = traumaIds.Contains(x.Id)
            });
            return items.ToList();
        }
    }
}