﻿using System.Collections.Generic;
using SportClub.Configuration;

namespace SportClub.Logic.Services
{
    public abstract class BaseService<TModel>
    {
        protected BaseRepository Repository;

        protected BaseService()
        {
            Repository = new BaseRepository(ConfigurationManager.ConnectionString);
        }

        public abstract IReadOnlyCollection<TModel> GetAll();
    }
}