﻿using System.Collections.Generic;
using System.Linq;
using SportClub.Logic.Entities;
using SportClub.Logic.Models;

namespace SportClub.Logic.Services
{
    public class CompetitionService : BaseService<CompetitionModel>
    {
        public override IReadOnlyCollection<CompetitionModel> GetAll()
        {
            var genders = Repository.QueryMany<GenderEntity>(DbProcedure.Gender.Select);
            var skills = Repository.QueryMany<SkillEntity>(DbProcedure.Skill.Select);
            var competitions = Repository.QueryMany<CompetitionEntity>(DbProcedure.Competition.Select);

            var items = competitions.Select(x =>
            {
                var minSkillEntry = skills.First(c => c.Id == x.MinSkillId);
                var maxSkillEntry = skills.First(c => c.Id == x.MaxSkillId);
                var genderEntry = genders.First(c => c.Id == x.GenderId);

                return new CompetitionModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Date = x.Date,

                    MinSkillId = minSkillEntry.Id,
                    MinSkillName = minSkillEntry.Name,

                    MaxSkillId = maxSkillEntry.Id,
                    MaxSkillName = maxSkillEntry.Name,

                    MinAge = x.MinAge,
                    MaxAge = x.MaxAge,

                    GenderId = genderEntry.Id,
                    GenderName = genderEntry.Name
                };
            });

            return items.ToList();
        }

        public void Delete(int id)
        {
            var data = new { id = id };
            Repository.Execute(DbProcedure.Competition.DeleteById, data);
        }

        public void Update(CompetitionModel model, IReadOnlyCollection<CheckItemModel> athletes)
        {
            var data = new
            {
                model.Id,
                model.Name,
                model.Date,
                model.MinSkillId,
                model.MaxSkillId,
                model.MinAge,
                model.MaxAge,
                model.GenderId
            };
            Repository.Execute(DbProcedure.Competition.Update, data);
            Repository.Execute(DbProcedure.AthleteToCompetition.DeleteByCompetitionId, new { competitionId = model.Id });
            InsertAthletes(model.Id, athletes);
        }

        public void Create(CompetitionModel model, IReadOnlyCollection<CheckItemModel> athletes)
        {
            var data = new
            {
                model.Name,
                model.Date,
                model.MinSkillId,
                model.MaxSkillId,
                model.MinAge,
                model.MaxAge,
                model.GenderId
            };
            var competitionId = Repository.QuerySingle<int>(DbProcedure.Competition.Insert, data);
            InsertAthletes(competitionId, athletes);
        }

        private void InsertAthletes(int competitionId, IEnumerable<CheckItemModel> athletes)
        {
            foreach (var athlete in athletes)
            {
                Repository.Execute(
                    DbProcedure.AthleteToCompetition.Insert,
                    new AthleteToCompetitionEntity { CompetitionId = competitionId, AthleteId = athlete.Id }
                );
            }
        }
    }
}