﻿using System.Collections.Generic;
using SportClub.Logic.Entities;

namespace SportClub.Logic.Services
{
    public class GenderService : BaseService<GenderEntity>
    {
        public override IReadOnlyCollection<GenderEntity> GetAll()
        {
            return Repository.QueryMany<GenderEntity>(DbProcedure.Gender.Select);
        }
    }
}