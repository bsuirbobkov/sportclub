﻿namespace SportClub.Logic.Models
{
    public class CheckItemModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Checked { get; set; }
    }
}