﻿namespace SportClub.Logic.Models
{
    public class CoachModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public int GenderId { get; set; }
        public string GenderName { get; set; }
        public int Rating { get; set; }
    }
}