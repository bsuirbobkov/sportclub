﻿using System;

namespace SportClub.Logic.Models
{
    public class AthleteModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }

        public int GenderId { get; set; }
        public string GenderName { get; set; }

        public int SkillId { get; set; }
        public string SkillName { get; set; }

        public int? CoachId { get; set; }
        public string CoachName { get; set; }

        public DateTime BornDate { get; set; }
        public int Rating { get; set; }
    }
}