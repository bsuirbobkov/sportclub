﻿using System;

namespace SportClub.Logic.Models
{
    public class CompetitionModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }

        public int MinSkillId { get; set; }
        public string MinSkillName { get; set; }

        public int MaxSkillId { get; set; }
        public string MaxSkillName { get; set; }

        public int MinAge { get; set; }
        public int MaxAge { get; set; }

        public int GenderId { get; set; }
        public string GenderName { get; set; }
    }
}