﻿namespace SportClub.Logic.Models
{
    public class FilterModel
    {
        public int SkillId { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
    }
}