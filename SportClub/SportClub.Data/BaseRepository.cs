﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace SportClub.Logic
{
    public class BaseRepository
    {
        private readonly string connectionString;

        public BaseRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IReadOnlyCollection<T> SelectMany<T>(string table)
        {
            using (var db = new SqlConnection(connectionString))
            {
                var items = db.Query<T>($"SELECT * FROM {table}");
                return items?.ToList() ?? new List<T>();
            }
        }

        public void Execute(string procedureName, object data)
        {
            using (var db = new SqlConnection(connectionString))
            {
                db.Execute(procedureName, data, commandType: CommandType.StoredProcedure);
            }
        }

        public IReadOnlyCollection<T> QueryMany<T>(string procedureName, object data = null)
        {
            using (var db = new SqlConnection(connectionString))
            {
                var items = db.Query<T>(procedureName, data, commandType: CommandType.StoredProcedure);
                return items?.ToList() ?? new List<T>();
            }
        }

        public T QuerySingle<T>(string procedureName, object data = null)
        {
            using (var db = new SqlConnection(connectionString))
            {
                return db.QueryFirstOrDefault<T>(procedureName, data, commandType: CommandType.StoredProcedure);
            }
        }
    }
}