﻿namespace SportClub.Logic
{
    public static class DbProcedure
    {
        public static class Athlete
        {
            public const string DeleteById = "[Athlete_DeleteById]";
            public const string GetById = "[Athlete_DeleteById]";
            public const string Insert = "[Athlete_Insert]";
            public const string Select = "[Athlete_Select]";
            public const string Update = "[Athlete_Update]";
            public const string Filter = "[Athlete_Filter]";
        }

        public static class AthleteToTrauma
        {
            public const string GetByAthleteId = "[AthleteToTrauma_GetByAthleteId]";
            public const string DeleteByAthleteId = "[AthleteToTrauma_DeleteByAthleteId]";
            public const string Insert = "[AthleteToTrauma_Insert]";
        }

        public static class AthleteToCompetition
        {
            public const string GetByCompetitionId = "[AthleteToCompetition_GetByCompetitionId]";
            public const string DeleteByCompetitionId = "[AthleteToCompetition_DeleteByCompetitionId]";
            public const string Insert = "[AthleteToCompetition_Insert]";
        }

        public static class Gender
        {
            public const string Select = "[Gender_Select]";
        }

        public static class Skill
        {
            public const string Select = "[Skill_Select]";
        }

        public static class Coach
        {
            public const string DeleteById = "[Coach_DeleteById]";
            public const string GetById = "[Coach_DeleteById]";
            public const string Insert = "[Coach_Insert]";
            public const string Select = "[Coach_Select]";
            public const string Update = "[Coach_Update]";
        }

        public static class Competition
        {
            public const string DeleteById = "[Competition_DeleteById]";
            public const string GetById = "[Competition_DeleteById]";
            public const string Insert = "[Competition_Insert]";
            public const string Select = "[Competition_Select]";
            public const string Update = "[Competition_Update]";
        }

        public static class Trauma
        {
            public const string DeleteById = "[Trauma_DeleteById]";
            public const string GetById = "[Trauma_DeleteById]";
            public const string Insert = "[Trauma_Insert]";
            public const string Select = "[Trauma_Select]";
            public const string Update = "[Trauma_Update]";
        }
    }
}